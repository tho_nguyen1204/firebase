class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def firebase
    base_uri = 'https://pqgq8jyyfu6.firebaseio-demo.com/'
    firebase = Firebase::Client.new(base_uri)
    firebase
  end

end
